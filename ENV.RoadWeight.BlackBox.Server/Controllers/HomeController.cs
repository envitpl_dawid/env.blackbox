﻿using ENV.RoadWeight.BlackBox.Server.Hubs;
using ENV.RoadWeight.BlackBox.Server.Logic.Enums;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 

namespace ENV.RoadWeight.BlackBox.Server.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<BlackBoxHub>();
            context.Clients.All.TurnOn(ChannelType.ChannelOne);

            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Off()
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<BlackBoxHub>();
            context.Clients.All.TurnOff(ChannelType.ChannelOne);

            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
