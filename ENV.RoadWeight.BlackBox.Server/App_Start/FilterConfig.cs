﻿using System.Web;
using System.Web.Mvc;

namespace ENV.RoadWeight.BlackBox.Server
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
