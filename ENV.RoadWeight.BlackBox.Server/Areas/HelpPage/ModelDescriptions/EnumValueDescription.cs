namespace ENV.RoadWeight.BlackBox.Server.Areas.HelpPage.ModelDescriptions
{
    public class EnumValueDescription
    {
        public string Documentation { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}