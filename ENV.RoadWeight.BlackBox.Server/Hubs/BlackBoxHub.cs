﻿using ENV.RoadWeight.BlackBox.Server.Helpers;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;

namespace ENV.RoadWeight.BlackBox.Server.Hubs
{
    [HubName("BlackBoxHub")]
    public class BlackBoxHub : Hub
    {
        public override Task OnConnected()
        {
            var clientDeviceIdentifier = Context.QueryString[Const.DeviceIdentifier];

            //Clients.Caller.notifyWrongVersion();


            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //using (var db = new UserContext())
            //{
            //    var connection = db.Connections.Find(Context.ConnectionId);
            //    connection.Connected = false;
            //    db.SaveChanges();
            //}
            return base.OnDisconnected(stopCalled);
        }
    }
}