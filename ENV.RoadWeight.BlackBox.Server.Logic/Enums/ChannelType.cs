﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENV.RoadWeight.BlackBox.Server.Logic.Enums
{
    public enum ChannelType
    {
        ChannelOne,
        ChannelTwo,
        ChannelThree
    }
}
