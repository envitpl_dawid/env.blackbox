﻿using Autofac;
using ENV.RoadWeight.BlackBox.Logic.Enums;
using ENV.RoadWeight.BlackBox.Logic.Helpers;
using ENV.RoadWeight.BlackBox.Logic.Interfaces;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ENV.RoadWeight.BlackBox.RBP3.Client.HubConnectors
{
    public class PowerSupplyHubConnector : IPowerSupplyHubConnector
    {
        #region Properties

        private IConfigurationService ConfigurationService { get; set; }

        private IPowerSupplyService PowerSupplyService { get; set; }

        private HubConnection Connection { get; set; }

        #endregion

        #region Constructors

        public PowerSupplyHubConnector()
        {
            ConfigurationService = App.Container.Resolve<IConfigurationService>(); ;
            PowerSupplyService = App.Container.Resolve<IPowerSupplyService>(); ;
        }

        #endregion

        #region Public functions

        public async Task RegisterHub()
        {
            var querystringData = new Dictionary<string, string>();
            querystringData.Add(Const.DeviceIdentifier, ConfigurationService.GetSettingValue(Const.DeviceIdentifier));
            Connection = new HubConnection(ConfigurationService.GetSettingValue(Const.HubServerPath), querystringData);
            var hubProxy = Connection.CreateHubProxy(ConfigurationService.GetSettingValue(Const.PowerSupplyHubName));
            var connection = Connection.Start();

            hubProxy.On<ChannelType>("TurnOn", TurnOnChannel);
            hubProxy.On<ChannelType>("TurnOff", TurnOffChannel);
            hubProxy.On<ChannelType>("Reboot", RebootChannel);


            await connection;
        }

        public void CloseConnections()
        {
            Connection.Stop();
        }

        #endregion

        #region Private functions

        private void TurnOnChannel(ChannelType channel)
        {
            PowerSupplyService.TurnOn(channel);
        }

        private void TurnOffChannel(ChannelType channel)
        {
            PowerSupplyService.TurnOff(channel);
        }

        private async void RebootChannel(ChannelType channel)
        {
            PowerSupplyService.TurnOff(channel);
            int delay = int.Parse(ConfigurationService.GetSettingValue(Const.RebootDelay));
            await Task.Delay(TimeSpan.FromSeconds(delay));
            PowerSupplyService.TurnOn(channel);
        }

        private void TurnOnnAllChannels()
        {
            PowerSupplyService.TurnOnnAllChannels();
        }

        #endregion
    }
}
