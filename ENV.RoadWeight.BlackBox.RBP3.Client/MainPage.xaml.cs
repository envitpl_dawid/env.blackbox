﻿using ENV.RoadWeight.BlackBox.Logic.Interfaces;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Autofac;
using ENV.RoadWeight.BlackBox.RBP3.Client.HubConnectors;
using Windows.UI.Xaml;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ENV.RoadWeight.BlackBox.RBP3.Client
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        #region Properties

        private IPowerSupplyService PowerSupplyService { get; set; }
        private IPowerSupplyHubConnector PowerSupplyHubConnector { get; set; }

        #endregion

        #region Constructors

        public MainPage()
        {
            this.InitializeComponent();
            PowerSupplyService = App.Container.Resolve<IPowerSupplyService>();
            PowerSupplyHubConnector = App.Container.Resolve<IPowerSupplyHubConnector>();
            TurnOnnAllChannels();
        }

        #endregion

        #region Protected functions

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            PowerSupplyHubConnector.RegisterHub();
        }

        #endregion

        #region Private functions

        private void TurnOnnAllChannels()
        {
            PowerSupplyService.TurnOnnAllChannels();
        }

        #endregion
    }
}
