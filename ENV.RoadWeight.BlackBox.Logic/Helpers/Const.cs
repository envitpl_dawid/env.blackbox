﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENV.RoadWeight.BlackBox.Logic.Helpers
{
    public static class Const
    {
        public static string HubServerPath { get => "HubServerUrl"; }
        public static string PowerSupplyHubName { get => "PowerSupplyHubName"; }
        public static string DeviceIdentifier { get => "DeviceIdentifier"; }
        public static string RebootDelay { get => "RebootDelay"; }
    }
}
