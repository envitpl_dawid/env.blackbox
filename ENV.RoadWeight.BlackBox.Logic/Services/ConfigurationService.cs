﻿using ENV.RoadWeight.BlackBox.Logic.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ENV.RoadWeight.BlackBox.Logic.Services
{
    public class ConfigurationService : IConfigurationService
    {
        #region Fields

        private string settingsPath;

        #endregion

        #region Constructors

        public ConfigurationService()
        {
            this.settingsPath = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, @"AppSettings.xml");
        }

        #endregion

        #region Pubic functions

        public Hashtable GetAppSettings()
        {
            var settings = LoadSettingsFromFile(this.settingsPath);
            return settings;
        }

        public string GetSettingValue(string settingName)
        {
            var settings = GetAppSettings();
            return (string)(settings[settingName] ?? "");
        }

        #endregion

        #region Private functions

        private Hashtable LoadSettingsFromFile(string path)
        {
            Hashtable returnTable = new Hashtable();
            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)))
                {
                    XmlDocument doc = new XmlDocument();
                    string xmlIn = reader.ReadToEnd();
                    doc.LoadXml(xmlIn);
                    foreach (XmlNode child in doc.ChildNodes)
                    {
                        if (child.Name.Equals("Settings"))
                        {
                            foreach (XmlNode node in child.ChildNodes)
                            {
                                if (node.Name.Equals("add"))
                                {
                                    returnTable.Add(node.Attributes["key"].Value, node.Attributes["value"].Value);
                                }
                            }
                        }
                    }
                }
            }
            return (returnTable);
        }

        #endregion
    }
}
