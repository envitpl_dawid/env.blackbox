﻿using ENV.RoadWeight.BlackBox.Logic.Enums;
using ENV.RoadWeight.BlackBox.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

namespace ENV.RoadWeight.BlackBox.Logic.Services
{
    public class PowerSupplyService : IPowerSupplyService
    {
        #region Fields

        private GpioPin channelOneGpioPin;
        private GpioPin channelTwoGpioPin;
        private GpioPin channelThreeGpioPin;

        #endregion

        #region Constructors

        public PowerSupplyService()
        {
            GpioController gpioController = GpioController.GetDefault();
            if(gpioController == null)
            {
                return;
            }
            channelOneGpioPin = gpioController.OpenPin(ChannelPin.Channel1);
            channelTwoGpioPin = gpioController.OpenPin(ChannelPin.Channel2);
            channelThreeGpioPin = gpioController.OpenPin(ChannelPin.Channel3);
        }

        #endregion

        #region Public functions

        public bool TurnOn(ChannelType channelType)
        {
            try
            {
                var selectedPin = GetGpioPIN(channelType);
                selectedPin.SetDriveMode(GpioPinDriveMode.Output);
                selectedPin.Write((GpioPinValue)PowerStatus.On);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool TurnOff(ChannelType channelType)
        {
            try
            {
                var selectedPin = GetGpioPIN(channelType);
                selectedPin.SetDriveMode(GpioPinDriveMode.Output);
                selectedPin.Write((GpioPinValue)PowerStatus.Off);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool TurnOnnAllChannels()
        {
            try
            {
                List<ChannelType> channels = new List<ChannelType>()
                {
                    ChannelType.ChannelOne,
                    ChannelType.ChannelTwo,
                    ChannelType.ChannelThree
                };

                foreach (ChannelType channelType in channels)
                {
                    var selectedPin = GetGpioPIN(channelType);
                    selectedPin.SetDriveMode(GpioPinDriveMode.Output);
                    selectedPin.Write((GpioPinValue)PowerStatus.On);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Private functions

        private GpioPin GetGpioPIN(ChannelType channelType)
        {
            switch (channelType)
            {
                case ChannelType.ChannelOne:
                    return channelOneGpioPin;
                case ChannelType.ChannelTwo:
                    return channelTwoGpioPin;
                case ChannelType.ChannelThree:
                    return channelThreeGpioPin;
            }
            return null;
        }

        #endregion
    }
}
