﻿using ENV.RoadWeight.BlackBox.Logic.Enums;

namespace ENV.RoadWeight.BlackBox.Logic.Interfaces
{
    public interface IPowerSupplyService
    {
        bool TurnOn(ChannelType channelType);
        bool TurnOff(ChannelType channelType);
        bool TurnOnnAllChannels();
    }
}
