﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENV.RoadWeight.BlackBox.Logic.Interfaces
{
    public interface IConfigurationService
    {
        Hashtable GetAppSettings();
        string GetSettingValue(string settingName);
    }
}
