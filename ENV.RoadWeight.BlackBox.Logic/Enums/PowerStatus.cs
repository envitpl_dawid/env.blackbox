﻿using Windows.Devices.Gpio;

namespace ENV.RoadWeight.BlackBox.Logic.Enums
{
    public enum PowerStatus
    {
        On = GpioPinValue.Low,
        Off = GpioPinValue.High
    }
}
